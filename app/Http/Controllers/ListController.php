<?php

namespace App\Http\Controllers;

use App\Models\Todolist;
use Illuminate\Http\Request;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $todolist = Todolist::get();
        $todolist = Todolist::paginate(5);
        return view('main', compact('todolist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'todo'=> 'required',
            'deadline'=> 'required',
            'description'=> 'required|max:255',
        ]);
        

        // save data
        Todolist::create([
            'todo' => $request->todo,
            'deadline' => $request->deadline,
            'description' => $request->description,
            'status' => 'In process'
        ]);


        return redirect('/')->with('success', "{$request->todo} was created successfully");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = Todolist::find($id);
        return view('edit', compact('list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todolist = Todolist::findOrFail($id);

        // dd($todolist);
        $form_data = $request->validate([
            'todo' => 'required',
            'deadline' => 'required',
            'description'=> 'required|max:255',
        ]);
        
        $todolist->todo = $form_data['todo'];
        $todolist->deadline = $form_data['deadline'];
        $todolist->description = $form_data['description'];
        
        $todolist->save();
        
        return redirect('/')->with('success', "{$todolist->todo} was updated successfully");

    }



    public function checkStatus($id)
    {
        $todolist = Todolist::findOrFail($id);

        $todolist->status = 'Done';
        $todolist->save();

        return redirect()->back();

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todolist = Todolist::find($id);
        $todolist->delete();
        return redirect()->back();
    }


    public function searching(Request $request){
        if($request->search){
            $todolist = Todolist::where('todo','like','%'.$request->search.'%')
            ->orWhere('description','like','%'.$request->search.'%')
            ->orWhere('status','like','%'.$request->search.'%')

            ->paginate(5);
            return view('main',compact('todolist'));
        }

        $todolist  =Todolist::paginate(5);
        return view('main',compact('todolist'));
       
    }


}

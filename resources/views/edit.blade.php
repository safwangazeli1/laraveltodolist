<!-- header -->
@include('layouts.header')
<!-- header end  -->


<body>

    <!-- nav -->
    @include('layouts.nav')
    <!-- nav end -->

    <div class="row mt-3 ml-5">
        <div class="col">
            <a href="/">
                <button class="btn btn-primary">Back</button>
            </a>
        </div>
        <div class="col">

        </div>
    </div>

    <div class="card mr-5 ml-5 mt-3 shadow p-3 mb-5 bg-white rounded">

        <form action="{{route('list.update',[$list->id])}}" method="POST" enctype="multipart/form-data">@csrf
            {{method_field('PUT')}}


            <div class="container mt-2">

                <div class="row">
                    <div class="col-7">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Todo</label>
                            <input type="text" name="todo" class="form-control" value="{{$list->todo}}"
                                placeholder="Enter todo here..." required>
                            <div class="invalid-feedback">
                                Please insert task.
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Deadline</label>
                            <input type="date" name="deadline" class="form-control" value="{{$list->deadline}}"
                                placeholder="Enter date" required>
                            <div class="invalid-feedback">
                                Please give some date.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <textarea class="form-control" name="description" id="validationCustom03" cols="5 " rows="5"
                            placeholder="Enter description here..." required>{{$list->description}}</textarea>
                        <div class="invalid-feedback">
                            Please give some description.
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success mt-3">Submit</button>
            </div>

        </form>
    </div>
</body>

</html>

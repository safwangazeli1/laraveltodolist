<!-- header -->
@include('layouts.header')
<!-- header end  -->

<body>

    <!-- nav -->
    @include('layouts.nav')
    <!-- nav end -->

    <div class="row mt-3 ml-5">
        <div class="col">
            <a href="/">
                <button class="btn btn-primary">Back</button>
            </a>
        </div>
        <div class="col">

        </div>
    </div>

    <div class="card mr-5 ml-5 mt-3 shadow p-3 mb-5 bg-white rounded">

        <form class="needs-validation" action="{{route('list.store')}}" method="POST" enctype="multipart/form-data"
            novalidate>
            @csrf

            <div class="container mt-2">

                <div class="row">
                    <div class="col-7">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Todo</label>
                            <input type="text" name="todo" class="form-control" placeholder="Enter todo here..."
                                required>
                            <div class="invalid-feedback">
                                Please insert task.
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Deadline</label>
                            <input type="date" name="deadline" class="form-control" required>
                            <div class="invalid-feedback">
                                Please give some date.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <textarea class="form-control" name="description" id="validationCustom03" cols="5 " rows="5"
                            placeholder="Enter description here..." required></textarea>
                        <div class="invalid-feedback">
                            Please give some description.
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success mt-3">Submit</button>
            </div>

        </form>
    </div>

</body>

</html>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<!-- header -->
@include('layouts.header')
<!-- header end -->

<style>
    #action_button {
        display: grid;
        justify-content: center;
        align-content: center;

        gap: 4px;
        grid-auto-flow: column;
    }
</style>


<body class="bg-light" id="page-top">

    <div id="wrapper">

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">

                <!-- TopBar -->
                <nav class="navbar navbar-dark bg-dark">
                    <a class="navbar-brand ml-2" href="/">Laravel Todolist</a>
                    <a href="{{route('list.create')}}">
                        <button class="btn btn-primary">Add Now</button>
                    </a>
                </nav>
                <!-- Topbar -->


                <!-- Container Fluid-->

                @if( session('success'))
                <div class="row mt-2">
                    <div class="col-7">

                    </div>
                    <div class="col-5">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                @endif

                
                <div class="card mr-5 ml-5 mt-3 shadow p-3 mb-3 bg-white rounded">

                    <form action="{{route('list.searching')}}" method="GET">

                        <div class="row mt-3 mb-3">
                            <div class="col-sm ml-5 mt-2">
                                <h3><strong>Todolist</strong></h3>
                            </div>
                            <div class="col-sm-4 mr-3 ">
                                <div class="input-group">
                                    <input type="text" name="search" class="form-control rounded" placeholder="Search"
                                        aria-label="Search" aria-describedby="search-addon" />
                                    <button type="submitsubmit" class="btn btn-outline-primary">Search</button>
                                    <a href="/">
                                        <button class="btn btn-primary ml-1">Reset</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col">No</th>
                                    <th scope="col">To Do</th>
                                    <th scope="col">Deadline</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>


                                </tr>
                            </thead>
                            <tbody>
                                @if(count($todolist)>0)
                                @foreach($todolist as $key => $list)

                                <tr>
                                    <th class="text-center" scope="row"> {{ $todolist->firstItem() + $key }}</th>
                                    <td>
                                        <strong>{{$list->todo}}</strong><br>
                                        <p>{{$list->description}}</p>
                                    </td>
                                    <td class="text-center">
                                        <span class="badge badge-pill badge-secondary">
                                            <?php
                                        $d=strtotime($list->deadline);
                                        echo date("d-m-Y", $d);
                                        ?>
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        @if( $list->status == "In process")
                                        <span class="badge badge-pill badge-info">In process</span>
                                        @else
                                        <span class="badge badge-pill badge-success">Done</span>
                                        @endif
                                    </td>
                                    <td>

                                        <div class="row" id="action_button">

                                            @if( $list->status != "Done")
                                            <form action="{{route('list.checkStatus',[$list->id])}}" method="POST"
                                                onsubmit="return confirmDone()">@csrf
                                                <button type="submit" class="bi bi-check2 btn btn-success"></button>
                                            </form>

                                            <a href="{{route('list.edit',[$list->id])}}">
                                                <button class="bi bi-pencil btn btn-info"></button>
                                            </a>

                                            <form action="{{route('list.destroy',[$list->id])}}" method="POST"
                                                onsubmit="return confirmDelete()">@csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="bi bi-trash btn btn-danger"></button>
                                            </form>

                                            @else
                                            <form action="{{route('list.destroy',[$list->id])}}" method="POST"
                                                onsubmit="return confirmDelete()">@csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="bi bi-trash btn btn-danger"></button>
                                            </form>
                                            @endif

                                        </div>

                                    </td>

                                </tr>
                                @endforeach
                                @else
                                <td class="text-center" colspan="5">There are no task</td>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2">
                        {{$todolist->links()}}
                    </div>
                </div>


                <br><br>
                <br><br>
                <br><br>

                <!---Container Fluid-->


                <!-- Footer -->
                @include('layouts.fotter')
                <!-- Footer -->

            </div>
        </div>

        <script>
            function confirmDelete() {
                return confirm('Are you sure you want to delete?');
            }

            function confirmDone() {
                return confirm('Are you sure you want to finish this task?');
            }
        </script>
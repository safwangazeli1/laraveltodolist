<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [ListController::class, 'index']);

// Route::get('/add', [ListController::class, 'create'])->name('list.create');

Route::post('/checkStatus/{checkStatus}', [ListController::class, 'checkStatus'])->name('list.checkStatus');

Route::get('/searching', [ListController::class, 'searching'])->name('list.searching');

Route::resource('list', ListController::class);



